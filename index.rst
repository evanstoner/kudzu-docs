.. Kudzu documentation master file, created by
   sphinx-quickstart on Sun Dec 18 17:01:09 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kudzu's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   intro/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
