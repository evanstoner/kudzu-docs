Key Terms
=========

Testbed
-------

These concepts relate to topological aspects of a Testbed's definition.

Testbed
  A container for Hosts, Networks, and Links. Testbeds target a specific Provider.

Host
  A host in the typical sense: server, machine, client, device, etc. Users assign
  Hosts a name, and an Image and Flavor from the Testbed's Provider.

Network
  A layer 2 broadcast domain and its layer 3 address space. In other words, it's
  like a switch and a single subnet that lives on it. Users assign Networks a
  name and an address space in CIDR form (e.g. 192.168.1.0/24).

  A special Service Network is created in every Testbed, which every Host is
  attached to. Its address space is configured by an administrator.

Link
  A connection from a Host to a Network. The user assigns the IP address of the
  link, which must fall inside the Network's address space.

Router
  Every testbed contains one Router, which Kudzu creates automatically on Testbed
  creation. The Router holds information about how to access Hosts once they are
  instantiated.

  There are several types of routers, which are discussed in detail in :doc:`../testbeds/routers`.

Provider
--------

These concepts relate to the IaaS resources used to realize a Testbed.

Provider
  A place where a Testbed is instantiated, typically a virtualization resource,
  but not necessarily. A Provider is responsible for specifying the Testbed Driver
  (and its configuration) to use for Testbed creation, and the Router Driver
  (and its configuration) to use for facilitating Host access.

  Example Providers may be:

    - "VMware Dev Host" with an ``Esxi`` Testbed Driver and a ``Gateway`` Router Driver
    - "Our Private Cloud" with an ``OpenStack`` Testbed Driver and an ``OpenStackPublicIp``
      Router Driver
    - "Local VirtualBox" with a ``VboxManage`` TestbedDriver and a ``Flat`` Router
      Driver

  Additionally, multiple providers may be specified for different configurations
  of the same drivers, for instance to accommodate a cloud with multiple regions
  or availability zones, or different parameters for creating a router's VPN endpoint.

Image
  The "base disk" to boot a Host from. Images may also have a username, password,
  and private key set to facilitate access (by the user or by a Provisioner).
  The Provider is responsible for listing all of the Images it has available,
  which Kudzu makes available to the user when defining the Testbed.

Flavor
  The hardware configuration, including CPUs, RAM, and disk space, assigned to
  a Host. Like Images, the Provider must report which Flavors are available. For
  Providers that support configurable hardware, such as VirtualBox, an administrator
  will have to set some pre-defined flavors.

Provision
---------

These concepts relate to the automation tools used to configure a Host once it
has been instantiated on a Provider.

Provisioner
  A tool capable of provisioning an instantiated Host, i.e. bootstrapping and
  configuring it. A Provisioner doesn't itself contain provisioning logic, it just
  provides the means to do so. Provisioners are automation tools like Ansible, Chef,
  Puppet, and Salt; and also basic tools like SSH and Provider-specific tools like
  VirtualBox Guest Additions. Like a Provider, a Provisioner specifies a Provision
  Driver and its configuration.

Persona
  A specific provisioning process which is run by a Provisioner, such as configuring
  a proxy or installing Apache, which is assigned to a Host. A Persona is always
  acquired from a *source*, like Git or HTTP (or most simply, the Filesystem).
  Kudzu will pull the latest version of the Persona.

  Since Personas are configurable, a default configuration can be set, which will
  be used by Hosts when no overriding configuration is specified. The format of
  the Persona's configuration is specific to its Provisioner.

Persona Assignment
  The assignment of a Persona to a particular Host, along with a priority (which
  controls relative execution order in the testbed), and a configuration. If no
  configuration is specified, the default configuration from the Persona will be
  used. The Provisioner driver may or may not support merging logic.

Build
-----

These concepts relate to the instantiation of a Testbed, including building the
topological components, configuring remote access with the Router, and provisioning
Hosts according to their Persona Assignments.

Instance
  Literally an *instance* of a defined resource. Instances are created for all of
  the following:

  - Testbed
  - Host
  - Network
  - Link
  - Router
  - Persona Assignment

  Since up until now all of the described resources were just prescriptive definitions,
  an Instance is the actual, concrete realization of that definition. There can
  be many Instances of a resource definition. If it helps, think of Instances
  as builds of a project on a continuous integration server.

  Instances have the following statuses:

  - ``NONE`` The instance hasn't begun being built yet.
  - ``IN_PROGRESS`` The instance is being built.
  - ``ACTIVE`` The instance has been built. It may or may not be "ready", i.e. "reachable" or "up".
  - ``DESTROYING`` The instance is being destroyed.
  - ``DESTROYED`` The instance has been destroyed.
  - ``ERROR`` Some error has occurred during any of the other states.

Build
  The lifecycle container for the instantiation, configuration, and destruction
  of a Testbed and all of its related resources. Builds have a start time, and
  also an expiration time. When a Build expires, its Instances are destroyed. A
  Testbed can have many simultaneous Builds.

Tenancy
-------

These concepts are "meta" concepts regarding how testbeds are organized in Kudzu.

Project
  A container for Testbeds. Every Testbed belongs to a Project.
