Overview
========

Kudzu is an IaaS and automation abstraction layer for cyber experimentation.
Let's dissect the components of that statement before going any further:

IaaS
  Infrastructure-as-a-service, i.e. a service that allows you to build virtual
  machines and virtual networks on-demand. Amazon Web Services, OpenStack, VirtualBox,
  and VMware are all examples of IaaS providers, at least in Kudzu's eyes.

Automation
  Tools like Ansible, Chef, Puppet, Salt, and others that help you consistently
  configure new virtual machines instantiated on an IaaS provider, since they
  will likely start in a "clean slate" or "vanilla" state.

Abstraction Layer
  You speak to Kudzu in the same terms, regardless of where you are building your
  resources. We take care of figuring out all the specific API calls and command
  line arguments necessary to build what you asked for.

Cyber Experimentation
  Kudzu builds virtual testbeds that allow researchers, developers, and testers
  to test whatever configurations of systems and software that are interesting
  to them.

Workflow
--------

An experimenter starts out by defining a testbed. When the testbed is defined,
nothing is created outside of Kudzu---e.g. virtual machines---we just record the
*concept* of the thing you want to build so that we can instantiate it later.
This makes the definition very fast, versus the time it wold take to build the
testbed while instantiating the resources in real time.

Part of the testbed definition involves virtual machines, networks, and links; the
other part involves assigning personas to the virtual machines so that they can
be configured to do something interesting to the experimenter.

Once a testbed is defined, the resources are created on a provider. Once those
resources are instantiated, the VMs are provisioned according to the personas assigned
to them. A testbed can be instantiated many times, but only on the same provider.
The personas can be configured or disabled each time the testbed is built, to test
various configurations.

When a testbed instantiation is destroyed, it has no effect on the testbed design.
And since the testbed is (hopefully) fully configured using personas, it can
be reinstantiated again at any time.
